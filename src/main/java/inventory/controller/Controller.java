package inventory.controller;

import inventory.service.InventoryService;

public interface Controller {
    // Setter
    void setService(InventoryService service);
}