package inventory.service;

import inventory.model.*;
import inventory.repository.IRepository;
import inventory.repository.InventoryRepository;
import javafx.collections.ObservableList;

public class InventoryService {

    private IRepository repo;

    // Constructor
    public InventoryService(IRepository repo){
        this.repo =repo;
    }

    /** Adds an Inhouse Part into repository
     *
     * @param name - name of Part
     * @param price - price of Part
     * @param inStock - stock of Part
     * @param min - min storage of Part
     * @param max - max storgae of Part
     * @param partDynamicValue - machineId
     */
    public void addInhousePart(String name, double price, int inStock, int min, int  max, int partDynamicValue){
        InhousePart inhousePart = new InhousePart(repo.getAutoPartId(), name, price, inStock, min, max, partDynamicValue);
        repo.addPart(inhousePart);
    }

    /** Adds an Outsource Part into repository
     *
     * @param name - name of Part
     * @param price - price of Part
     * @param inStock - stock of Part
     * @param min - min storage of Part
     * @param max - max storgae of Part
     * @param partDynamicValue - company name
     */
    public void addOutsourcePart(String name, double price, int inStock, int min, int  max, String partDynamicValue){
        OutsourcedPart outsourcedPart = new OutsourcedPart(repo.getAutoPartId(), name, price, inStock, min, max, partDynamicValue);
        repo.addPart(outsourcedPart);
    }

    /**
     * Adds a Part into repository
     * @param part - part to be added
     */
    public void addPart(Part part){
        repo.addPart(part);
    }

    /** Adds a Product into repository
     *
     * @param name - name of Product
     * @param price - price of Product
     * @param inStock - stock of Product
     * @param min - min storage of Product
     * @param max - max storage of Product
     * @param addParts - list of component Parts
     */
    public void addProduct(String name, double price, int inStock, int min, int  max, ObservableList<Part> addParts){
        Product product = new Product(repo.getAutoProductId(), name, price, inStock, min, max, addParts);
        repo.addProduct(product);
    }

    /** Returns all Parts form repository
     *
     * @return ObservableList of Parts
     */
    public ObservableList<Part> getAllParts() {
        return repo.getAllParts();
    }

    /** Returns all Products form repository
     *
     * @return ObservableList of Products
     */
    public ObservableList<Product> getAllProducts() {
        return repo.getAllProducts();
    }

    /** Searches a Part in the repository after name
     *
     * @param search - searching parameter
     * @return found Part
     */
    public Part lookupPart(String search) {
        return repo.lookupPart(search);
    }

    /** Searches a Product in the repository after name
     *
     * @param search - searching parameter
     * @return found Product
     */
    public Product lookupProduct(String search) {
        return repo.lookupProduct(search);
    }

    /** Updates an Inhouse Part into repository
     *
     * @param partIndex - index of part
     * @param partId - id of part
     * @param name - new name of Part
     * @param price - new price of Part
     * @param inStock - new stock of Part
     * @param min - new min storage of Part
     * @param max - new max storgae of Part
     * @param partDynamicValue - new machineId
     */
    public void updateInhousePart(int partIndex, int partId, String name, double price, int inStock, int min, int max, int partDynamicValue){
        InhousePart inhousePart = new InhousePart(partId, name, price, inStock, min, max, partDynamicValue);
        repo.updatePart(partIndex, inhousePart);
    }

    /** Updates an Outsourced Part into repository
     *
     * @param partIndex - index of Part
     * @param partId - id of Part
     * @param name - new name of Part
     * @param price - new price of Part
     * @param inStock - new stock of Part
     * @param min - new min storage of Part
     * @param max - new max storgae of Part
     * @param partDynamicValue - new company name
     */
    public void updateOutsourcedPart(int partIndex, int partId, String name, double price, int inStock, int min, int max, String partDynamicValue){
        OutsourcedPart outsourcedPart = new OutsourcedPart(partId, name, price, inStock, min, max, partDynamicValue);
        repo.updatePart(partIndex, outsourcedPart);
    }

    /** Updates a Product into repository
     *
     * @param productIndex - index of Product
     * @param productId - id of Product
     * @param name - new name of Product
     * @param price - new price of Product
     * @param inStock - new stock of Product
     * @param min - new min storage of Product
     * @param max - new max storage of Product
     * @param addParts - new list of component Parts
     */
    public void updateProduct(int productIndex, int productId, String name, double price, int inStock, int min, int max, ObservableList<Part> addParts){
        Product product = new Product(productId, name, price, inStock, min, max, addParts);
        repo.updateProduct(productIndex, product);
    }

    /** Deletes a Part from repository
     *
     * @param part - Part to be deleted
     */
    public void deletePart(Part part){
        repo.deletePart(part);
    }

    /** Deletes a Product from repository
     *
     * @param product - Product to be deleted
     */
    public void deleteProduct(Product product){
        repo.deleteProduct(product);
    }

}
