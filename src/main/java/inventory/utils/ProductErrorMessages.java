package inventory.utils;

public class ProductErrorMessages {
    public static final String EMPTY_NAME = "A name has not been entered. ";
    public static final String NEGATIVE_PRICE = "The price must be greater than $0. ";
    public static final String NEGATIVE_MIN_STOCK = "The inventory level must be greater than 0. ";
    public static final String MIN_STOCK_GREATER_THAN_MAX_STOCK = "The Min value must be less than the Max value. ";
    public static final String STOCK_SMALLER_THAN_MIN_STOCK = "Inventory level is lower than minimum value. ";
    public static final String STOCK_GREATER_THAN_MAX_STOCK = "Inventory level is higher than the maximum value. ";
    public static final String EMPTY_PART_LIST = "Product must contain at least 1 part. ";
    public static final String PRODUCT_PRICE_SMALLER_THAN_PARTS_TOTAL_PRICE = "Product price must be greater than cost of parts. ";
}
