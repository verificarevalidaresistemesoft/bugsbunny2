package inventory.repository;


import inventory.model.*;
import inventory.model.validator.PartValidator;
import inventory.model.validator.ProductValidator;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.*;
import java.util.StringTokenizer;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class InventoryRepository implements IRepository{

	private static String filename = "data/items.txt";
	private PartValidator partValidator = new PartValidator();
	private ProductValidator productValidator = new ProductValidator();
	private static final Logger logger= LogManager.getLogger();

	// Declare fields
	private ObservableList<Product> products;
	private ObservableList<Product> productsCopy;
	private ObservableList<Part> parts;
	private ObservableList<Part> partsCopy;
	private int autoPartId;
	private int autoProductId;

	// Constructor
	public InventoryRepository(){
		this.products = FXCollections.observableArrayList();
		this.productsCopy = FXCollections.observableArrayList();
		this.parts= FXCollections.observableArrayList();
		this.partsCopy= FXCollections.observableArrayList();
		this.autoProductId=0;
		this.autoPartId=0;

		readParts();
		readProducts();
		setCopy();
	}

	public void setCopy(){
		for(Part p : parts)
			partsCopy.add(p);

		for(Product p : products)
			productsCopy.add(p);
	}

	/**
	 * Reads Parts from file
	 */
	public void readParts(){
		ClassLoader classLoader = InventoryRepository.class.getClassLoader();
		File file = new File(classLoader.getResource(filename).getFile());
		try(BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line = null;
			while((line=br.readLine())!=null){
				Part part=getPartFromString(line);
				if (part!=null)
					parts.add(part);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** Transforms file line into a Part
	 *
	 * @param line - line in file
	 * @return Part associated with given line
	 */
	private Part getPartFromString(String line){
		Part item=null;
		if (line==null|| line.equals("")) return null;
		StringTokenizer st=new StringTokenizer(line, ",");
		String type=st.nextToken();
		if (type.equals("I")) {
			int id= Integer.parseInt(st.nextToken());
			autoPartId = id;
			String name= st.nextToken();
			double price = Double.parseDouble(st.nextToken());
			int inStock = Integer.parseInt(st.nextToken());
			int minStock = Integer.parseInt(st.nextToken());
			int maxStock = Integer.parseInt(st.nextToken());
			int idMachine= Integer.parseInt(st.nextToken());
			item = new InhousePart(id, name, price, inStock, minStock, maxStock, idMachine);
		}
		if (type.equals("O")) {
			int id= Integer.parseInt(st.nextToken());
			autoPartId = id;
			String name= st.nextToken();
			double price = Double.parseDouble(st.nextToken());
			int inStock = Integer.parseInt(st.nextToken());
			int minStock = Integer.parseInt(st.nextToken());
			int maxStock = Integer.parseInt(st.nextToken());
			String company=st.nextToken();
			item = new OutsourcedPart(id, name, price, inStock, minStock, maxStock, company);
		}
		return item;
	}

	/**
	 * Reads Products from file
	 */
	public void readProducts(){
		ClassLoader classLoader = InventoryRepository.class.getClassLoader();
		File file = new File(classLoader.getResource(filename).getFile());

		try(BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line = null;
			while((line=br.readLine())!=null){
				Product product=getProductFromString(line);
				if (product!=null)
					products.add(product);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** Transforms a file line into a Product
	 *
	 * @param line - line in file
	 * @return Product associated with given line
	 */
	private Product getProductFromString(String line){
		Product product=null;
		if (line==null|| line.equals("")) return null;
		StringTokenizer st=new StringTokenizer(line, ",");
		String type=st.nextToken();
		if (type.equals("P")) {
			int id= Integer.parseInt(st.nextToken());
			autoProductId = id;
			String name= st.nextToken();
			double price = Double.parseDouble(st.nextToken());
			int inStock = Integer.parseInt(st.nextToken());
			int minStock = Integer.parseInt(st.nextToken());
			int maxStock = Integer.parseInt(st.nextToken());
			String partIDs=st.nextToken();

			StringTokenizer ids= new StringTokenizer(partIDs,":");
			ObservableList<Part> list= FXCollections.observableArrayList();
			while (ids.hasMoreTokens()) {
				String idP = ids.nextToken();
				Part part = lookupPart(idP);
				if (part != null)
					list.add(part);
			}
			product = new Product(id, name, price, inStock, minStock, maxStock, list);
			product.setAssociatedParts(list);
		}
		return product;
	}

	/**
	 * Writes all Parts and Products into file
	 */
	public void writeAll() {

		ClassLoader classLoader = InventoryRepository.class.getClassLoader();
		File file = new File(classLoader.getResource(filename).getFile());

		try(BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
			for (Part p:parts) {
				logger.info(p.toString());
				bw.write(p.toString());
				bw.newLine();
			}

			for (Product pr:products) {
				String line=pr.toString()+",";
				ObservableList<Part> list= pr.getAssociatedParts();
				int index=0;
				while(index<list.size()-1){
					line=line+list.get(index).getPartId()+":";
					index++;
				}
				if (index==list.size()-1)
					line=line+list.get(index).getPartId();
				bw.write(line);
				bw.newLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/** Adds a Part
	 *
	 * @param part - part to be added
	 */
	@Override
	public void addPart(Part part){
		partValidator.isValidPart(part.getName(),part.getPrice(),part.getInStock(),part.getMin(),part.getMax());
		parts.add(part);
		writeAll();
	}

	/** Adds a Product
	 *
	 * @param product - product to be added
	 */
	public void addProduct(Product product){
		productValidator.isValidProduct(product.getName(),product.getPrice(),product.getInStock(),product.getMin(),product.getMax(),product.getAssociatedParts());
		products.add(product);
		writeAll();
	}

	/** Generates a new Id for a new Part
	 *
	 * @return next id for parts
	 */
	public int getAutoPartId(){
		autoPartId++;
		return autoPartId;
	}

	/** Generates a new Id for a new Product
	 *
	 * @return next id for products
	 */
	public int getAutoProductId(){
		autoProductId++;
		return autoProductId;
	}

	/** Returns all Parts
	 *
	 * @return ObservableList of Parts
	 */
	public ObservableList<Part> getAllParts(){
		return parts;
	}

	/** Returns all Products
	 *
	 * @return ObservableList of Products
	 */
	public ObservableList<Product> getAllProducts(){
		return products;
	}

	/** Searches a Part after name
	 *
	 * @param search - searching parameter
	 * @return found Part
	 */
	@Override
	public Part lookupPart (String search){
		if (search.equals(""))
			throw new RepositoryException("Search string is null.");

		for(int i = 0; i < getSize(); i++) {
			Part p = getAllParts().get(i);
			if(p.getName().contains(search))
				return p;
			if ((p.getPartId()+"").equals(search))
				return p;
		}

		return null;
	}

	/** Searches a Product after name
	 *
	 * @param search - searching parameter
	 * @return found Product
	 */
	public Product lookupProduct (String search){
		for(Product p: products) {
			if(p.getName().contains(search) || (p.getProductId()+"").equals(search)) return p;
		}
		return null;
	}

	/** Updates the Part with partIndex index
	 *
	 * @param partIndex - index of Part to be updated
	 * @param part - new Part
	 */
	public void updatePart(int partIndex, Part part){
		partValidator.isValidPart(part.getName(),part.getPrice(),part.getInStock(),part.getMin(),part.getMax());
		parts.set(partIndex, part);
		writeAll();
	}

	/** Updates the Product with productIndex index
	 *
	 * @param productIndex - index of Product to be updated
	 * @param product - new Product
	 */
	public void updateProduct(int productIndex, Product product){
		productValidator.isValidProduct(product.getName(),product.getPrice(),product.getInStock(),product.getMin(),product.getMax(),product.getAssociatedParts());
		products.set(productIndex, product);
		writeAll();
	}

	/** Deletes a Part
	 *
	 * @param part - Part to be deleted
	 */
	public void deletePart(Part part){
		parts.remove(part);
		writeAll();
	}

	/** Deletes a Product
	 *
	 * @param product - Product to be deleted
	 */
	public void deleteProduct(Product product){
		products.remove(product);
		writeAll();
	}

	/**
	 * Sets the new size of the parts list
	 * @param n - new size of the list
	 */
	public void setSizeParts(int n){
		parts.clear();
		for (int i=0;i<n;i++){
			parts.add(partsCopy.get(i));
		}
	}

	/**
	 * Resets the parts list to the initial one
	 */
	public void resetParts(){
		parts = partsCopy;
	}

	public void resetProducts(){
		products = productsCopy;
	}

	@Override
	public int getSize(){
		return parts.size();
	}

	@Override
	public Part getLastPart(){
		return parts.get(parts.size() - 1);
	}

	@Override
	public void resetElements(){
		try {
			ClassLoader classLoader = InventoryRepository.class.getClassLoader();
			File file = new File(classLoader.getResource(filename).getFile());
			new FileOutputStream(file).close();

			resetParts();
			resetProducts();
			writeAll();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}