package inventory.repository;

public class RepositoryException extends RuntimeException {
    // Constructor
    public RepositoryException (String message) {
        super(message);
    }
}

