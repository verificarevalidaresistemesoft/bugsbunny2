package inventory.model.validator;

public class ValidationException extends RuntimeException {
    // Constructor
    public ValidationException (String message) {
        super(message);
    }
}

