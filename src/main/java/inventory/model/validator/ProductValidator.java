package inventory.model.validator;

import inventory.model.Part;
import javafx.collections.ObservableList;

import static inventory.utils.ProductErrorMessages.*;

public class ProductValidator {

    /**
     * Generate an error message for invalid values in a product
     * and evaluate whether the sum of the price of associated parts
     * is less than the price of the resulting product.
     * A valid product will return an empty error message string.
     * @param name
     * @param min
     * @param max
     * @param inStock
     * @param price
     * @param parts
     * @return
     */
    public void isValidProduct(String name, double price, int inStock, int min, int max, ObservableList<Part> parts) {
        String errorMessage = "";
        double sumOfParts = 0.00;
        for (int i = 0; i < parts.size(); i++) {
            sumOfParts += parts.get(i).getPrice();
        }
        if (name.equals("")) {
            errorMessage += EMPTY_NAME;
        }
        if (min < 0) {
            errorMessage += NEGATIVE_MIN_STOCK;
        }
        if (price < 0.01) {
            errorMessage += NEGATIVE_PRICE;
        }
        if (min > max) {
            errorMessage += MIN_STOCK_GREATER_THAN_MAX_STOCK;
        }
        if(inStock < min) {
            errorMessage += STOCK_SMALLER_THAN_MIN_STOCK;
        }
        if(inStock > max) {
            errorMessage += STOCK_GREATER_THAN_MAX_STOCK;
        }
        if (parts.size() < 1) {
            errorMessage += EMPTY_PART_LIST;
        }
        if (sumOfParts > price) {
            errorMessage += PRODUCT_PRICE_SMALLER_THAN_PARTS_TOTAL_PRICE;
        }

        if (errorMessage.length() > 0)
            throw new ValidationException(errorMessage);
    }
}
