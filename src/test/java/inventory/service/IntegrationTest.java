package inventory.service;

import inventory.model.InhousePart;
import inventory.model.OutsourcedPart;
import inventory.model.validator.ValidationException;
import inventory.repository.IRepository;
import inventory.repository.InventoryRepository;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class IntegrationTest {

    private static IRepository inventoryRepository;
    private static InventoryService inventoryService;
    private static InhousePart inhousePart;
    private static OutsourcedPart outsourcedPart;

    @BeforeAll
    static void setUp() {
        inventoryRepository = new InventoryRepository();
        inventoryService = new InventoryService(inventoryRepository);
    }

    @AfterAll
    static void tearDown(){
        inventoryRepository.resetElements();
    }

    @Test
    void addInhousePart() {
        inhousePart = new InhousePart(100, "Test", 10.9, 5, 2, 10, 12);
        int size = inventoryRepository.getSize();

        assertDoesNotThrow(() -> inventoryService.addPart(inhousePart));

        assertEquals(size + 1, inventoryRepository.getSize());
        assertEquals(inhousePart, inventoryRepository.getLastPart());
    }

    @Test
    void addOutsourcePart() {
        outsourcedPart = new OutsourcedPart(101, "Test", 10.9, 1, 2, 10, "Test Co");

        Throwable exception = assertThrows(ValidationException.class, () -> inventoryService.addPart(outsourcedPart));
        assertEquals("Inventory level is lower than minimum value. ", exception.getMessage());
    }

    @Test
    void lookupPart() {
        inhousePart = new InhousePart(100, "Test", 10.9, 5, 2, 10, 12);

        inventoryService.addPart(inhousePart);

        assertEquals(inhousePart, inventoryService.lookupPart("Test"));
    }
}