package inventory.service;

import inventory.model.InhousePart;
import inventory.model.OutsourcedPart;
import inventory.model.validator.ValidationException;
import inventory.repository.InventoryRepository;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class ServiceRepositoryIntegrationTest {
    private static InventoryRepository inventoryRepository;
    private static InventoryService inventoryService;
    private static InhousePart inhousePart;
    private static OutsourcedPart outsourcedPart;

    @BeforeAll
    static void setUp() {
        inhousePart = mock(InhousePart.class);
        outsourcedPart = mock(OutsourcedPart.class);
        inventoryRepository = new InventoryRepository();
        inventoryService = new InventoryService(inventoryRepository);
    }

    @AfterAll
    static void tearDown(){
        inventoryRepository.resetElements();
    }

    @Test
    void addInhousePart() {
        Mockito.when(inhousePart.getPartId()).thenReturn(12);
        Mockito.when(inhousePart.getName()).thenReturn("Test");
        Mockito.when(inhousePart.getPrice()).thenReturn(20.3);
        Mockito.when(inhousePart.getInStock()).thenReturn(21);
        Mockito.when(inhousePart.getMin()).thenReturn(10);
        Mockito.when(inhousePart.getMax()).thenReturn(30);
        Mockito.when(inhousePart.getMachineId()).thenReturn(1140);

        int size = inventoryRepository.getSize();

        assertDoesNotThrow(() -> inventoryService.addPart(inhousePart));

        assertEquals(size+1,inventoryRepository.getSize());
        assertEquals(inhousePart,inventoryRepository.getLastPart());
    }

    @Test
    void addOutsourcePart() {
        Mockito.when(outsourcedPart.getPartId()).thenReturn(12);
        Mockito.when(outsourcedPart.getName()).thenReturn("Test");
        Mockito.when(outsourcedPart.getPrice()).thenReturn(-20.3);
        Mockito.when(outsourcedPart.getInStock()).thenReturn(21);
        Mockito.when(outsourcedPart.getMin()).thenReturn(10);
        Mockito.when(outsourcedPart.getMax()).thenReturn(30);
        Mockito.when(outsourcedPart.getCompanyName()).thenReturn("TestCompany");

        Throwable exception = assertThrows(ValidationException.class, () -> inventoryService.addPart(outsourcedPart));
        assertEquals("The price must be greater than 0. ", exception.getMessage());
    }

    @Test
    void lookupPart() {
        Mockito.when(inhousePart.getPartId()).thenReturn(12);
        Mockito.when(inhousePart.getName()).thenReturn("Test");
        Mockito.when(inhousePart.getPrice()).thenReturn(20.3);
        Mockito.when(inhousePart.getInStock()).thenReturn(21);
        Mockito.when(inhousePart.getMin()).thenReturn(10);
        Mockito.when(inhousePart.getMax()).thenReturn(30);
        Mockito.when(inhousePart.getMachineId()).thenReturn(1140);

        inventoryService.addPart(inhousePart);

        assertEquals(inhousePart, inventoryService.lookupPart("Test"));
    }
}