package inventory.service;

import inventory.model.InhousePart;
import inventory.model.OutsourcedPart;
import inventory.model.Part;
import inventory.model.validator.ValidationException;
import inventory.repository.IRepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class ServiceUnitTest {

    private static IRepository repository;
    private static InventoryService inventoryService;

    @BeforeEach
    public void setUp(){
        repository = mock(IRepository.class);
        inventoryService = new InventoryService(repository);
    }

    @AfterAll
    static void tearDown(){
        repository.resetElements();
    }

    @Test
    void addInhousePart() {
        Part p = new InhousePart(0,"Name",20.5,4,1,5,241);
        Mockito.when(repository.getAutoPartId()).thenReturn(0);
        Mockito.doNothing().when(repository).addPart(p);

        inventoryService.addInhousePart("Name",20.5,4,1,5,241);

        Mockito.verify(repository, times(1)).getAutoPartId();
        Mockito.verify(repository, times(1)).addPart(p);
    }

    @Test
    void addOutsourcePart() {
        Part p = new OutsourcedPart(0,"Name",-20.5,4,1,5,"Company");
        Mockito.when(repository.getAutoPartId()).thenReturn(0);
        Mockito.doThrow(new ValidationException("The price must be greater than 0. ")).when(repository).addPart(p);

        try{
            inventoryService.addOutsourcePart("Name",-20.5,4,1,5,"Company");
        } catch (ValidationException e) {
            Assertions.assertTrue(e instanceof ValidationException);
            Assertions.assertTrue(e.getMessage().equals("The price must be greater than 0. "));
        }
    }

    @Test
    void lookupPart() {
        Part p = new InhousePart(0,"Name",20.5,4,1,5,241);
        Part p2 = new OutsourcedPart(1,"Name2",30.5,4,1,5,"Company");
        ObservableList<Part> parts = FXCollections.observableArrayList(p,p2);

        Mockito.when(repository.getAllParts()).thenReturn(parts);
        Mockito.when(repository.getSize()).thenReturn(2);
        Mockito.when(repository.lookupPart("Name")).thenReturn(p);

        assertEquals(p,inventoryService.lookupPart("Name"));

        Mockito.verify(repository, times(1)).lookupPart("Name");
    }
}