package inventory.service;

import inventory.model.InhousePart;
import inventory.model.Part;
import inventory.model.validator.ValidationException;
import inventory.repository.InventoryRepository;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static inventory.utils.ProductErrorMessages.*;
import static org.junit.jupiter.api.Assertions.*;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class InventoryServiceTest {
    private static InventoryRepository inventoryRepository;
    private static InventoryService inventoryService;
    private String name, companyName;
    private double price;
    private int stock, min, max, machineId;
    private String productName;
    private int productInStock, productMin, productMax;
    private double productPrice;
    private ObservableList<Part> productAssociatedParts;

    @BeforeAll
    static void setUp() {
        inventoryRepository = new InventoryRepository();
        inventoryService = new InventoryService(inventoryRepository);
    }

    @AfterAll
    static void tearDown(){
        inventoryRepository.resetElements();
    }

    void setUpPart() {
        name = "Test";
        companyName = "Test Company";
        price = 10;
        stock = 5;
        min = 2;
        max = 10;
        machineId = 113;
    }

    void setUpProduct() {
        productName = "Test";
        productInStock = 12;
        productMin = 3;
        productMax = 15;
        productPrice = 4.5;
        productAssociatedParts = FXCollections.observableArrayList();
        productAssociatedParts.add(new InhousePart(1001, "Test", 0, 14, 4, 40, 12));
    }

    @Test
    @DisplayName("Successful ECP test for adding inhouse part. Price should be a positive number")
    @Order(1)
    void addInhousePartECPSuccessPricePositiveNumber() {
        setUpPart();
        assertDoesNotThrow(() -> inventoryService.addInhousePart(name, price, stock, min, max, machineId));
    }

    @Test
    @DisplayName("Failed ECP test for adding inhouse part. Price is a negative number")
    void addInhousePartECPFailedPriceNegativeNumber() {
        setUpPart();
        price = -10;
        Throwable exception = assertThrows(ValidationException.class, () -> inventoryService.addInhousePart(name, price, stock, min, max, machineId));
        assertEquals("The price must be greater than 0. ", exception.getMessage());
    }

    @Test
    @DisplayName("Successful ECP test for adding outsource part. Price should be a positive number")
    void addOutsourcePartECPSuccessPricePositiveNumber() {
        setUpPart();
        assertDoesNotThrow(() -> inventoryService.addOutsourcePart(name, price, stock, min, max, companyName));
    }

    @Test
    @DisplayName("Failed ECP test for adding outsource part. Price is a negative number")
    void addOutsourcePartECPFailedPriceNegativeNumber() {
        setUpPart();
        price = -10;
        Throwable exception = assertThrows(ValidationException.class, () -> inventoryService.addOutsourcePart(name, price, stock, min, max, companyName));
        assertEquals("The price must be greater than 0. ", exception.getMessage());
    }

    @Test
    @DisplayName("Successful BVA test for adding inhouse part. Price is greater than zero")
    void addInhousePartBVASuccessPriceGreaterThanZero() {
        setUpPart();
        price = 0.1;
        assertDoesNotThrow(() -> inventoryService.addInhousePart(name, price, stock, min, max, machineId));
    }

    @Test
    @DisplayName("Failed BVA test for adding inhouse part. Price is equal to zero")
    void addInhousePartBVAFailedPriceZero() {
        setUpPart();
        price = 0;
        Throwable exception = assertThrows(ValidationException.class, () -> inventoryService.addInhousePart(name, price, stock, min, max, machineId));
        assertEquals("The price must be greater than 0. ", exception.getMessage());
    }

    @Test
    @DisplayName("Failed BVA test for adding inhouse part. Price is less than zero")
    void addInhousePartBVAFailedPriceLessThanZero() {
        setUpPart();
        price = -0.1;
        Throwable exception = assertThrows(ValidationException.class, () -> inventoryService.addInhousePart(name, price, stock, min, max, machineId));
        assertEquals("The price must be greater than 0. ", exception.getMessage());
    }

    @Test
    @DisplayName("Successful BVA test for adding outsource part. Price is greater than zero")
    void addOutsourcePartBVASuccessPriceGreaterThanZero() {
        setUpPart();
        price = 0.1;
        assertDoesNotThrow(() -> inventoryService.addOutsourcePart(name, price, stock, min, max, companyName));
    }

    @Test
    @DisplayName("Failed BVA test for adding outsource part. Price is equal to zero")
    void addOutsourcePartBVAFailedPriceZero() {
        setUpPart();
        price = 0;
        Throwable exception = assertThrows(ValidationException.class, () -> inventoryService.addOutsourcePart(name, price, stock, min, max, companyName));
        assertEquals("The price must be greater than 0. ", exception.getMessage());
    }

    @Test
    @DisplayName("Failed BVA test for adding outsource part. Price is less than zero")
    void addOutsourcePartBVAFailedPriceLessThanZero() {
        setUpPart();
        price = -0.1;
        Throwable exception = assertThrows(ValidationException.class, () -> inventoryService.addOutsourcePart(name, price, stock, min, max, companyName));
        assertEquals("The price must be greater than 0. ", exception.getMessage());
    }

    @Test
    @DisplayName("Successful ECP test for adding inhouse part. Stock should be a between min and max")
    void addInhousePartECPSuccessStockBetweenMinMax() {
        setUpPart();
        assertDoesNotThrow(() -> inventoryService.addInhousePart(name, price, stock, min, max, machineId));
    }

    @Test
    @DisplayName("Failed ECP test for adding inhouse part. Stock is greater than max")
    void addInhousePartECPFailedStockGreaterThanMax() {
        setUpPart();
        stock = 20;
        Throwable exception = assertThrows(ValidationException.class, () -> inventoryService.addInhousePart(name, price, stock, min, max, machineId));
        assertEquals("Inventory level is higher than the maximum value. ", exception.getMessage());
    }

    @Test
    @DisplayName("Failed ECP test for adding inhouse part. Stock is less than min")
    void addInhousePartECPFailedStockLessThanMin() {
        setUpPart();
        stock = 1;
        Throwable exception = assertThrows(ValidationException.class, () -> inventoryService.addInhousePart(name, price, stock, min, max, machineId));
        assertEquals("Inventory level is lower than minimum value. ", exception.getMessage());
    }

    @Test
    @DisplayName("Successful ECP test for adding outsource part. Stock should be a between min and max")
    void addOutsourcePartECPSuccessStockBetweenMinMax() {
        setUpPart();
        assertDoesNotThrow(() -> inventoryService.addOutsourcePart(name, price, stock, min, max, companyName));
    }

    @Test
    @DisplayName("Failed ECP test for adding outsource part. Stock is greater than max")
    void addOutsourcePartECPFailedStockGreaterThanMax() {
        setUpPart();
        stock = 20;
        Throwable exception = assertThrows(ValidationException.class, () -> inventoryService.addOutsourcePart(name, price, stock, min, max, companyName));
        assertEquals("Inventory level is higher than the maximum value. ", exception.getMessage());
    }

    @Test
    @DisplayName("Failed ECP test for adding outsource part. Stock is less than min")
    void addOutsourcePartECPFailedStockLessThanMin() {
        setUpPart();
        stock = 1;
        Throwable exception = assertThrows(ValidationException.class, () -> inventoryService.addOutsourcePart(name, price, stock, min, max, companyName));
        assertEquals("Inventory level is lower than minimum value. ", exception.getMessage());
    }

    @Test
    @DisplayName("Successful BVA test for adding inhouse part. Stock is less than max")
    void addInhousePartBVASuccessStockLessThanMax() {
        setUpPart();
        stock = max - 1;
        assertDoesNotThrow(() -> inventoryService.addInhousePart(name, price, stock, min, max, machineId));
    }

    @Test
    @DisplayName("Successful BVA test for adding inhouse part. Stock is equal to max")
    void addInhousePartBVASuccessStockIsMax() {
        setUpPart();
        stock = max;
        assertDoesNotThrow(() -> inventoryService.addInhousePart(name, price, stock, min, max, machineId));
    }

    @Test
    @DisplayName("Failed BVA test for adding inhouse part. Stock is greater than max")
    void addInhousePartBVAFailedStockGreaterThanMax() {
        setUpPart();
        stock = max + 1;
        Throwable exception = assertThrows(ValidationException.class, () -> inventoryService.addInhousePart(name, price, stock, min, max, machineId));
        assertEquals("Inventory level is higher than the maximum value. ", exception.getMessage());
    }

    @Test
    @DisplayName("Successful BVA test for adding inhouse part. Stock is greater than min")
    void addInhousePartBVASuccessStockGreaterThanMin() {
        setUpPart();
        stock = min + 1;
        assertDoesNotThrow(() -> inventoryService.addInhousePart(name, price, stock, min, max, machineId));
    }

    @Test
    @DisplayName("Successful BVA test for adding inhouse part. Stock is equal to min")
    void addInhousePartBVASuccessStockIsMin() {
        setUpPart();
        stock = min;
        assertDoesNotThrow(() -> inventoryService.addInhousePart(name, price, stock, min, max, machineId));
    }

    @Test
    @DisplayName("Failed BVA test for adding inhouse part. Stock is less than min")
    void addInhousePartBVAFailedStockLessThanMin() {
        setUpPart();
        stock = min - 1;
        Throwable exception = assertThrows(ValidationException.class, () -> inventoryService.addInhousePart(name, price, stock, min, max, machineId));
        assertEquals("Inventory level is lower than minimum value. ", exception.getMessage());
    }

    @Test
    @DisplayName("Successful BVA test for adding outsource part. Stock is less than max")
    void addOutsourcePartBVASuccessStockLessThanMax() {
        setUpPart();
        stock = max - 1;
        assertDoesNotThrow(() -> inventoryService.addOutsourcePart(name, price, stock, min, max, companyName));
    }

    @Test
    @DisplayName("Successful BVA test for adding outsource part. Stock is equal to max")
    void addOutsourcePartBVASuccessStockIsMax() {
        setUpPart();
        stock = max;
        assertDoesNotThrow(() -> inventoryService.addOutsourcePart(name, price, stock, min, max, companyName));
    }

    @Test
    @DisplayName("Failed BVA test for adding outsource part. Stock is greater than max")
    void addOutsourcePartBVAFailedStockGreaterThanMax() {
        setUpPart();
        stock = max + 1;
        Throwable exception = assertThrows(ValidationException.class, () -> inventoryService.addOutsourcePart(name, price, stock, min, max, companyName));
        assertEquals("Inventory level is higher than the maximum value. ", exception.getMessage());
    }

    @Test
    @DisplayName("Successful BVA test for adding outsource part. Stock is greater than min")
    void addOutsourcePartBVASuccessStockGreaterThanMin() {
        setUpPart();
        stock = min + 1;
        assertDoesNotThrow(() -> inventoryService.addOutsourcePart(name, price, stock, min, max, companyName));
    }

    @Test
    @DisplayName("Successful BVA test for adding outsource part. Stock is equal to min")
    void addOutsourcePartBVASuccessStockIsMin() {
        setUpPart();
        stock = min;
        assertDoesNotThrow(() -> inventoryService.addOutsourcePart(name, price, stock, min, max, companyName));
    }

    @Test
    @DisplayName("Failed BVA test for adding outsource part. Stock is less than min")
    @Disabled
    void addOutsourcePartBVAFailedStockLessThanMin() {
        setUpPart();
        stock = min - 1;
        Throwable exception = assertThrows(ValidationException.class, () -> inventoryService.addOutsourcePart(name, price, stock, min, max, companyName));
        assertEquals("Inventory level is lower than minimum value. ", exception.getMessage());
    }

    @ParameterizedTest
    @ValueSource(doubles = {50.0, 10.4, 78.9})
    @DisplayName("Successful ECP test for adding a product. Product's price should be a positive number")
    void addProductECPSuccessPricePositive(double price) {
        setUpProduct();
        productPrice = price;
        assertDoesNotThrow(() -> inventoryService.addProduct(productName, productPrice, productInStock, productMin, productMax, productAssociatedParts));
    }

    @ParameterizedTest
    @ValueSource(doubles = {-10, -50, -100})
    @DisplayName("Failed ECP test for adding a product. Product's price is a negative number")
    void addProductECPFailedPriceNegative(double price) {
        setUpProduct();
        productPrice = price;
        Throwable exception = assertThrows(ValidationException.class, () -> inventoryService.addProduct(productName, productPrice, productInStock, productMin, productMax, productAssociatedParts));
        assertEquals(NEGATIVE_PRICE + PRODUCT_PRICE_SMALLER_THAN_PARTS_TOTAL_PRICE, exception.getMessage());
    }

    @ParameterizedTest
    @ValueSource(doubles = {0.01, 0.05, 0.1})
    @DisplayName("Successful BVA test for adding a product. Product's price is greater than zero")
    void addProductBVASuccessPriceGreaterThanZero(double price) {
        setUpProduct();
        productPrice = price;
        assertDoesNotThrow(() -> inventoryService.addProduct(productName, productPrice, productInStock, productMin, productMax, productAssociatedParts));
    }

    @Test
    @DisplayName("Failed BVA test for adding a product. Product's price is equal to zero")
    void addProductBVAFailedPriceZero() {
        setUpProduct();
        productPrice = 0;
        Throwable exception = assertThrows(ValidationException.class, () -> inventoryService.addProduct(productName, productPrice, productInStock, productMin, productMax, productAssociatedParts));
        assertEquals(NEGATIVE_PRICE, exception.getMessage());
    }

    @ParameterizedTest
    @ValueSource(doubles = {-0.01, -0.05, -0.1})
    @DisplayName("Failed BVA test for adding a product. Product's price is less than zero")
    void addProductBVAFailedPriceLessThanZero(double price) {
        setUpProduct();
        productPrice = price;
        Throwable exception = assertThrows(ValidationException.class, () -> inventoryService.addProduct(productName, productPrice, productInStock, productMin, productMax, productAssociatedParts));
        assertEquals(NEGATIVE_PRICE + PRODUCT_PRICE_SMALLER_THAN_PARTS_TOTAL_PRICE, exception.getMessage());
    }

    @ParameterizedTest
    @ValueSource(ints = {5, 7,  10, 11})
    @DisplayName("Successful ECP test for adding a product. Product's inStock should be a value between min and max")
    void addProductECPSuccessInStockBetweenMinMax(int stock) {
        setUpProduct();
        productInStock = stock;
        assertDoesNotThrow(() -> inventoryService.addProduct(productName, productPrice, productInStock, productMin, productMax, productAssociatedParts));
    }

    @ParameterizedTest
    @ValueSource(ints = {50, 100, 150, 200})
    @DisplayName("Failed ECP test for adding a product. Product's inStock is greater than max")
    void addProductECPFailedInStockGreaterThanMax(int stock) {
        setUpProduct();
        productInStock = stock;
        Throwable exception = assertThrows(ValidationException.class, () -> inventoryService.addProduct(productName, productPrice, productInStock, productMin, productMax, productAssociatedParts));
        assertEquals(STOCK_GREATER_THAN_MAX_STOCK, exception.getMessage());
    }

    @Test
    @DisplayName("Failed ECP test for adding a product. Product's inStock is less than min")
    void addProductECPFailedInStockLessThanMin() {
        setUpProduct();
        productInStock = 1;
        Throwable exception = assertThrows(ValidationException.class, () -> inventoryService.addProduct(productName, productPrice, productInStock, productMin, productMax, productAssociatedParts));
        assertEquals(STOCK_SMALLER_THAN_MIN_STOCK, exception.getMessage());
    }

    @Test
    @DisplayName("Successful BVA test for adding a product. Product's inStock is less than max")
    void addProductBVASuccessInStockLessThanMax() {
        setUpProduct();
        productInStock = productMax - 1;
        assertDoesNotThrow(() -> inventoryService.addProduct(productName, productPrice, productInStock, productMin, productMax, productAssociatedParts));
    }

    @Test
    @DisplayName("Successful BVA test for adding a product. Product's inStock is equal to max")
    void addProductBVASuccessInStockIsMax() {
        setUpProduct();
        productInStock = productMax;
        assertDoesNotThrow(() -> inventoryService.addProduct(productName, productPrice, productInStock, productMin, productMax, productAssociatedParts));
    }

    @RepeatedTest(3)
    @DisplayName("Failed BVA test for adding a product. Product's inStock is greater than max")
    void addProductBVAFailedInStockGreaterThanMax() {
        setUpProduct();
        productInStock = productMax + 1;
        Throwable exception = assertThrows(ValidationException.class, () -> inventoryService.addProduct(productName, productPrice, productInStock, productMin, productMax, productAssociatedParts));
        assertEquals(STOCK_GREATER_THAN_MAX_STOCK, exception.getMessage());
    }

    @Test
    @DisplayName("Successful BVA test for adding a product. Product's inStock is greater than min")
    void addProductBVASuccessInStockGreaterThanMin() {
        setUpProduct();
        productInStock = productMin + 1;
        assertDoesNotThrow(() -> inventoryService.addProduct(productName, productPrice, productInStock, productMin, productMax, productAssociatedParts));
    }

    @Test
    @DisplayName("Successful BVA test for adding a product. Product's inStock is equal to min")
    void addProductBVASuccessInStockIsMin() {
        setUpProduct();
        productInStock = productMin;
        assertDoesNotThrow(() -> inventoryService.addProduct(productName, productPrice, productInStock, productMin, productMax, productAssociatedParts));
    }

    @Test
    @DisplayName("Failed BVA test for adding a product. Product's inStock is less than min")
    void addProductBVAFailedStockLessThanMin() {
        setUpProduct();
        productInStock = productMin - 1;
        Throwable exception = assertThrows(ValidationException.class, () -> inventoryService.addProduct(productName, productPrice, productInStock, productMin, productMax, productAssociatedParts));
        assertEquals(STOCK_SMALLER_THAN_MIN_STOCK, exception.getMessage());
    }
}