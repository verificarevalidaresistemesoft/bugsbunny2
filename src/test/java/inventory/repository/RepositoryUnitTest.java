package inventory.repository;

import inventory.model.InhousePart;
import inventory.model.OutsourcedPart;
import inventory.model.validator.ValidationException;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class RepositoryUnitTest {
    private static InventoryRepository inventoryRepository;
    private static InhousePart inhousePart;
    private static OutsourcedPart outsourcedPart;

    @BeforeAll
    static void setUp() {
        inhousePart = mock(InhousePart.class);
        outsourcedPart = mock(OutsourcedPart.class);
        inventoryRepository = new InventoryRepository();
    }

    @AfterAll
    static void tearDown(){
        inventoryRepository.resetElements();
    }

    @Test
    void addInhousePart() {
        Mockito.when(inhousePart.getPartId()).thenReturn(100);
        Mockito.when(inhousePart.getName()).thenReturn("Test");
        Mockito.when(inhousePart.getPrice()).thenReturn(10.9);
        Mockito.when(inhousePart.getInStock()).thenReturn(5);
        Mockito.when(inhousePart.getMin()).thenReturn(2);
        Mockito.when(inhousePart.getMax()).thenReturn(10);
        Mockito.when(inhousePart.getMachineId()).thenReturn(12);

        int size = inventoryRepository.getSize();

        assertDoesNotThrow(() -> inventoryRepository.addPart(inhousePart));

        assertEquals(size + 1,inventoryRepository.getSize());
        assertEquals(inhousePart, inventoryRepository.getLastPart());
    }

    @Test
    void addOutsourcePart() {
        Mockito.when(outsourcedPart.getPartId()).thenReturn(101);
        Mockito.when(outsourcedPart.getName()).thenReturn("Test");
        Mockito.when(outsourcedPart.getPrice()).thenReturn(10.9);
        Mockito.when(outsourcedPart.getInStock()).thenReturn(1);
        Mockito.when(outsourcedPart.getMin()).thenReturn(2);
        Mockito.when(outsourcedPart.getMax()).thenReturn(10);
        Mockito.when(outsourcedPart.getCompanyName()).thenReturn("Test Company");

        Throwable exception = assertThrows(ValidationException.class, () -> inventoryRepository.addPart(outsourcedPart));
        assertEquals("Inventory level is lower than minimum value. ", exception.getMessage());
    }

    @Test
    void lookupPart() {
        Mockito.when(inhousePart.getPartId()).thenReturn(100);
        Mockito.when(inhousePart.getName()).thenReturn("Test");
        Mockito.when(inhousePart.getPrice()).thenReturn(10.9);
        Mockito.when(inhousePart.getInStock()).thenReturn(5);
        Mockito.when(inhousePart.getMin()).thenReturn(2);
        Mockito.when(inhousePart.getMax()).thenReturn(10);
        Mockito.when(inhousePart.getMachineId()).thenReturn(12);

        inventoryRepository.addPart(inhousePart);

        assertEquals(inhousePart, inventoryRepository.lookupPart("Test"));
    }
}