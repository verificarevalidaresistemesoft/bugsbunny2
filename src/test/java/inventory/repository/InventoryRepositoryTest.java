package inventory.repository;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InventoryRepositoryTest {

    private static InventoryRepository inventoryRepository;

    @BeforeAll
    static void setUp() {
        inventoryRepository = new InventoryRepository();
    }

    @AfterAll
    static void tearDown(){
        inventoryRepository.resetElements();
    }

    @Test
    void lookupPartSearchStringNull() {
        inventoryRepository.setSizeParts(4);
        Throwable exception = assertThrows(RepositoryException.class, () -> inventoryRepository.lookupPart(""));
        assertEquals("Search string is null.", exception.getMessage());
    }

    @Test
    void lookupPartListEmpty() {
        inventoryRepository.setSizeParts(0);
        assertEquals(null,inventoryRepository.lookupPart("Cog"));
    }

    @Test
    void lookupPartSearchStringIsName() {
        inventoryRepository.setSizeParts(4);
        assertEquals(1,inventoryRepository.lookupPart("Cog").getPartId());
    }

    @Test
    void lookupPartSearchById(){
        inventoryRepository.setSizeParts(4);
        assertEquals(2, inventoryRepository.lookupPart("2").getPartId());
    }

    @Test
    void lookupPartSearchNameNotFound(){
        inventoryRepository.setSizeParts(4);
        assertNull(inventoryRepository.lookupPart("Test"));
    }
}