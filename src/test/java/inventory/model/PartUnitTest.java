package inventory.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PartUnitTest {

    void verify(Part p){
        assertEquals(1, p.getPartId());
        assertEquals("Name", p.getName());
        assertEquals(10.3, p.getPrice());
        assertEquals(5, p.getInStock());
        assertEquals(2, p.getMin());
        assertEquals(100, p.getMax());
    }

    @Test
    void createInhousePart() {
        InhousePart part = new InhousePart(1,"Name",10.3,5,2,100,1004);
        verify(part);
        assertEquals(1004,part.getMachineId());
    }

    @Test
    void createOutsourcePart() {
        OutsourcedPart part = new OutsourcedPart(1,"Name",10.3,5,2,100,"Company");
        verify(part);
        assertEquals("Company",part.getCompanyName());
    }
}